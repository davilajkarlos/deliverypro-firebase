import { boot } from 'quasar/wrappers'

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

// var firebaseConfig = {
//   apiKey: process.env.FB_APIKEY,
//   authDomain: process.env.FB_AUTHDOMAIN,
//   projectId: process.env.FB_PROJECTID,
//   storageBucket: process.env.FB_STORAGEBUCKET,
//   messagingSenderId: process.env.FB_MESSAGINGSENDERID,
//   appId: process.env.FB_APPID,
//   measurementId: process.env.FB_MEASUREMENTID
//   // apiKey: "AIzaSyDk2OzF8TzwkIyerrg--XXcY-KnFs0-y7U",
//   // authDomain: "deliverypro-firebase.firebaseapp.com",
//   // projectId: "deliverypro-firebase",
//   // storageBucket: "deliverypro-firebase.appspot.com",
//   // messagingSenderId: "8180014816",
//   // appId: "1:8180014816:web:ec8e8a1ba0ffacf315ba85",
//   // measurementId: "G-7F04DT1KY9"
// };
// var firebaseConfig = {
//   apiKey: "AIzaSyA7QIM_lqMDcQz4eqnajDdCvBq_2PqinYM",
//   authDomain: "delivery-pro-9f79a.firebaseapp.com",
//   projectId: "delivery-pro-9f79a",
//   storageBucket: "delivery-pro-9f79a.appspot.com",
//   messagingSenderId: "20674432204",
//   appId: "1:20674432204:web:6f43bace66b66e92700669"
// };

var firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID,
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();
const timeServer = firebase.firestore.FieldValue.serverTimestamp;

export { db, auth, timeServer }

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async( /* { app, router, ... } */ ) => {
  // something to do
})